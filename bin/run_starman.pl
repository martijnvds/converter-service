#! /usr/bin/env perl

# This needs to run its "INIT" - including it from the converter plugin/app is too late.
use Glib::Object::Introspection;

# Run starman as usual
require '/usr/local/bin/starman';

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.