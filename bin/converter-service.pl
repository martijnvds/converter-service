#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";

use ConvertService::Web;
use Plack::Builder;

builder {
    ConvertService::Web->to_app;
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.
