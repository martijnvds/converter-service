# Basics
requires 'Cpanel::JSON::XS';
requires 'JSON::MaybeXS';
requires 'File::Slurp';
requires 'Moo';
requires 'Try::Tiny';
requires 'namespace::autoclean';
requires 'Function::Parameters';
requires 'IPC::System::Simple';

# For the client tool
requires 'LWP::UserAgent';
#requires 'LWP::ConsoleLogger';

# Web stuff
requires 'Dancer2';
requires 'Starman';

# Image/PDF manipulation
requires 'Imager';
requires 'Image::ExifTool';
requires 'PDF::API2';
requires 'Poppler';
requires 'Cairo';
requires 'Cairo::GObject';

# Recommended by Image::Exiftool, for better identification of
# "nested" file formats (odt, docx, etc.)
requires 'Archive::Zip';

on 'test' => sub {
    requires 'Test::Compile';
    requires 'Test::Simple';
};
