package ConvertService;
use Moo;
use namespace::autoclean;

=head1 NAME

ConvertService - File Conversion as a Service

=head1 DESCRIPTION

This class (and its plugins) provide a simple way to convert a file from
one format to another (PNG to GIF, PDF to PNG, etc.)

It can be used to create thumbnails from most file types, or to .

=cut

use Image::ExifTool qw(ImageInfo);
use Function::Parameters qw(:strict);
use Module::Pluggable::Object;

# Cache plugins on startup/load
my @PLUGINS = do {
    my $finder = Module::Pluggable::Object->new(
        search_path => 'ConvertService::Plugin',
        instantiate => 'new',
    );

    $finder->plugins;
};

fun _identify_mime_type($source_file) {
    my $info = ImageInfo($source_file, 'MIMEType');
    return $info->{'MIMEType'} unless $info->{Error};

    # Perl builtin: "File looks like text"
    return "text/plain" if -T $source_file;

    die sprintf(
        "Error determining MIME type of input: '%s'\n",
         $info->{Error} // "Unknown error"
    );
}

=head1 METHODS

=head2 convert(:$source_file, :$to_type, :$options)

Convert the file C<$source_file> to C<$to_type>, using C<$options> to
influence the conversion.

Most options are plugin-specific, but the following option is supported by the
main converter code:

=over

=item * force_from_type

MIME type to use for the source file, instead of the autodetected one.

Use this only if the autodetection code mis-detects your source file format.

=back

=cut

method convert(:$source_file, :$to_type, :$options) {
    my $from_type;
    if (   defined $options->{force_from_type}
        && $options->{force_from_type} =~ m{^[a-z0-9-]+/[a-z0-9.-]+$}
    ) {
        $from_type = $options->{force_from_type};
    }
    else {
         $from_type = _identify_mime_type($source_file);
    }

    # Check for a plugin of type $from_type -> $to_type
    for my $plugin (@PLUGINS) {
        if ($plugin->can_convert(from => $from_type, to => $to_type)) {
            my $result = $plugin->convert(
                source_file => $source_file,
                from_type   => $from_type,
                to_type     => $to_type,
                options     => $options,
            );

            return $result;
        }
    }

    die "I don't know how to convert from '$from_type' -> '$to_type'\n";
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.
