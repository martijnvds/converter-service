package ConvertService::Plugin;
use Moo::Role;

requires 'can_convert', 'convert';

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.