package ConvertService::Plugin::OfficeFormats;
use Moo;
use namespace::autoclean;
use autodie qw(:all);

with 'ConvertService::Plugin';

use Function::Parameters qw(:strict);
use File::Basename;
use File::Slurp;
use File::Temp;
use List::Util qw(any);
use Try::Tiny;

=head1 NAME

ConvertService::Plugin::OfficeText - ConvertService plugin for "office" formats

=head1 DESCRIPTION

This plugin allows ConvertService to convert from "office" style formats
(like OpenDocument, OpenOffice XML, plain text) to other (similar) formats or
PDF.

=cut

my %SUPPORTED_TARGET_TYPES = do {
    my %rv;

    my %SUPPORTED_SOURCE_TYPES = (
        text => [qw(
            text/plain
            text/html
            text/rtf
            application/msword
            application/vnd.oasis.opendocument.text
            application/vnd.openxmlformats-officedocument.wordprocessingml.document
        )],
        spreadsheet => [qw(
            text/csv
            text/plain
            application/vnd.ms-excel
            application/vnd.oasis.opendocument.spreadsheet
            application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        )],
    );

    for my $kind (keys %SUPPORTED_SOURCE_TYPES) {
        for my $type (@{ $SUPPORTED_SOURCE_TYPES{$kind} }) {
            # For any given source type, support conversion to all other source
            # types + PDF (excluding conversion to the current format)
            $rv{$kind}{$type} = [
                'application/pdf',
                grep { $_ ne $type } @{ $SUPPORTED_SOURCE_TYPES{$kind} }
            ];
        }
    }
    %rv;
};

my %SUFFIXES = (
    'application/msword'       => 'doc',
    'application/pdf'          => 'pdf',
    'application/vnd.ms-excel' => 'xls',
    'application/vnd.oasis.opendocument.spreadsheet' => 'ods',
    'application/vnd.oasis.opendocument.text'        => 'odt',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'       => 'xlsx',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
    'text/csv'           => 'csv',
    'text/html'          => 'html',
    'text/plain'         => 'txt',
    'text/rtf'           => 'rtf',
);

has unoconv_path => (
    is      => 'ro',
    default => '/usr/bin/unoconv',
);

=head2 can_convert

Returns a true value if this plugin can convert files with a MIME type of
C<$from> to C<$to>.

=cut

method can_convert(:$from, :$to) {
    for my $kind (keys %SUPPORTED_TARGET_TYPES) {
        if (any { $_ eq $to } @{ $SUPPORTED_TARGET_TYPES{$kind}{$from} }) {
            return 1;
        }
    }

    return 0;
}

=head2 convert(:$source_file, :$from_type, :$to_type, :$options)

Convert C<$source_file> to the format specified by C<$to_type>.

C<$options> should be a reference to a hash. This plugin understands the
following options:

=over

=item * column_types

An array of column types (one entry for each column in the CSV source file.

The column types can be anything, but the value C<datetime> is special: it
ensures the field is parsed as a date/timestamp field with the "yy/mm/dd" or
"yyyy-mm-dd" date format.

=item * locale

A locale string (of the form xx_YY), where C<xx> is the 2-letter ISO 639 code
for the language, and C<YY> the ISO 3166 country code.

This will affect the way numbers (decimals and thousand separators) in a CSV
input file are parsed.

=back

=cut

method convert(:$source_file, :$from_type, :$to_type, :$options) {
    my $destination_file = File::Temp->new(SUFFIX => ".$SUFFIXES{$to_type}");

    my @unoconv_args = ("-f" => $SUFFIXES{$to_type});

    push @unoconv_args, ("-e" => "SelectPdfVersion=1") if $to_type eq 'application/pdf';

    if (   exists $SUPPORTED_TARGET_TYPES{spreadsheet}{$from_type}
        && any { $_ eq $from_type } qw(text/plain text/csv)
    ) {
        # Comma-separated, double-quoted, UTF-8, start at first line
        my $field_options = "44,34,76,1";

        my @field_format = _process_field_format($options->{column_types});
        $field_options .= "," . join("/", @field_format) if (@field_format);

        push @unoconv_args, ("-i", "FilterOptions=$field_options");
    }

    # Prevent multiple unoconv processes from being started at the same
    # time (by different workers), it makes LibreOffice deadlock sometimes.
    my @unoconv_env = ("/usr/bin/flock" => "/tmp/unoconv.lock");

    if ($options->{locale} && $options->{locale} =~ /^(?<locale>[a-z]{2,3}_[A-Z]{2})/) {
        push @unoconv_env, ("/usr/bin/env" => "LC_ALL=$+{locale}.UTF-8");
    }

    my $tmpdir = File::Temp->newdir();

    my $source_link = "$tmpdir/" . basename($source_file) . ".$SUFFIXES{$from_type}";
    symlink("$source_file", $source_link);

    try {
        my @command = (
            @unoconv_env,
            $self->unoconv_path,
            @unoconv_args,
            "-o" => "$destination_file",
            $source_link,
        );

        system(@command);
    } catch {
        # Rethrow the exception -- the try is mainly here to ensure unlinking
        # of the temporary symlink
        die $_;
    } finally {
        unlink($source_link);
    };

    my $result = read_file("$destination_file", binmode => ':raw');
    return $result;
}

=head2 _process_field_format($column_types)

Parse an array (ref) of column type names and generate a list that can be fed
to Unoconv.

=cut

fun _process_field_format($column_types) {
    return if not $column_types;
    return if not ref($column_types);
    return if ref($column_types) ne 'ARRAY';

    my @field_format;

    for (my $col = 0; $col <= $#$column_types; $col++) {
        if ($column_types->[$col] eq 'datetime') {
            # "yy/mm/dd" timestamp field
            push @field_format, ("$col" => 5);
        } else {
            # "Standard" field formatting
            push @field_format, ("$col" => 1);
        }
    }

    return @field_format;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.
