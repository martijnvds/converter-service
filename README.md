# Converter Service

This is a simple service to convert files from one format to another,
optionally specifying options for resizing and parsing.

It is a Perl application that uses `Moo` and `Function::Parameters`,
with a `Dancer2` frontend.

The following formats are supported:

* Comma-separated values (CSV)
* GIF images
* JPEG images
* Open Office XML Documents (DOCX)
* Open Office XML Sheets (XLSX)
* OpenDocument Sheet (ODS)
* OpenDocument Text (ODT)
* Plain text (TXT)
* PNM images
* Portable Document Format (PDF)
* Portable Network Graphics (PNG)
* TIFF images
* Windows bitmaps (BMP)

# Web service API

The web service accepts HTTP POST requests with a JSON body.

## Endpoints

### POST /v1/convert

```json
{
    "to_type": "image/png",
    "content": "YmFzZTY0LWVuY29kZWQgYmluYXJ5IGltYWdlIGRhdGE=",
    "options": { "width": 800, "height": 600 }
}
```

* to_type - The MIME type to convert to.

    Generally, files can only be converted to "similar" formats:
    images to images, documents to documents, etc. An exception to this
    is PDF: most formats can be converted to PDF, and PDF can be turned
    into PNG.
* content - The content of the file to convert, base64-encoded
* options - A JSON object containing specific options for the conversion.

    These options are plugin-specific, their availability and use are described
    in each plugin's documentation.

# Writing a new file conversion plugin

To write a new plugin, create a new `.pm` file in the
`lib/ConvertService/Plugin` directory, make sure it consumes the
`ConvertService::Plugin` role, and implement the required methods.

# Build/testing instructions

```shell_session
$ docker build -t converter .
```

```shell_session
$ docker run --rm converter prove -l
```

For verbose test output, you can use use:

```shell_session
$ docker run --rm converter prove -lv
```
