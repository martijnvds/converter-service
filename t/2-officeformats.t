#! /usr/bin/env perl
use warnings;
use strict;

use Test::More;

use ConvertService::Plugin::OfficeFormats;
use Image::ExifTool qw(ImageInfo);
use FindBin;

my $csi = ConvertService::Plugin::OfficeFormats->new();

my @word_processor_types = qw(
    text/plain
    text/html
    text/rtf
    application/msword
    application/vnd.oasis.opendocument.text
    application/vnd.openxmlformats-officedocument.wordprocessingml.document
);

my @spreadsheet_types = qw(
    text/csv
    text/plain
    application/vnd.ms-excel
    application/vnd.oasis.opendocument.spreadsheet
    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
);

ok(
    !$csi->can_convert(from => 'application/msword', to => 'application/vnd.ms-excel'),
    "Cannot convert between 'text' and 'spreadsheet' types"
);

for my $source (@word_processor_types) {
    for my $dest (@word_processor_types, 'application/pdf') {
        if ($source eq $dest) {
            ok( 
                !$csi->can_convert(from => $source, to => $dest),
                "Cannot convert from $source to $dest"
            );
        }
        else {
            ok( 
                 $csi->can_convert(from => $source, to => $dest),
                "Can convert from $source to $dest"
            );
        }
        ok(
            !$csi->can_convert(from => "application/pdf", to => $dest),
            "Cannot convert from application/pdf to image/$dest"
        );
        ok(
            !$csi->can_convert(from => $source, to => "video/mp4"),
            "Cannot convert from $source to video/mp4"
        );
    }
}

for my $source (@spreadsheet_types) {
    for my $dest (@spreadsheet_types, 'application/pdf') {
        if ($source eq $dest) {
            ok( 
                !$csi->can_convert(from => $source, to => $dest),
                "Cannot convert from $source to $dest"
            );
        }
        else {
            ok( 
                 $csi->can_convert(from => $source, to => $dest),
                "Can convert from $source to $dest"
            );
        }
        ok(
            !$csi->can_convert(from => "application/pdf", to => $dest),
            "Cannot convert from application/pdf to image/$dest"
        );
        ok(
            !$csi->can_convert(from => $source, to => "video/mp4"),
            "Cannot convert from $source to video/mp4"
        );
    }
}

# Document conversion
{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/Word.docx",
        from_type   => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        to_type     => 'application/msword',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/msword', 'DOCX to DOC conversion succeeded');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/LibreOffice.doc",
        from_type   => 'application/vnd.oasis.opendocument.text',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'DOC to PDF conversion succeeded');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/LibreOffice.xls",
        from_type   => 'application/vnd.ms-excel',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'XLS to PDF conversion succeeded');
}

{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.csv",
        from_type   => 'text/plain',
        to_type     => 'application/vnd.ms-excel',
        options     => {
            locale => "nl_NL",
            column_types => [
                "datetime",
                "numeric",
                "string",
            ],
        },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/vnd.ms-excel', 'CSV to XLS conversion succeeded');
}


{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/Excel.xlsx",
        from_type   => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        to_type     => 'text/csv',
        options     => {
            locale => "nl_NL",
        },
    );

    is($converted, <<EOT, "XLS to CSV conversion succeeded");
Excel Document 1,
,
10,30
20,70
30,110
40,150
50,190
60,230
70,270
80,310
90,350
100,390
110,430
120,470
130,510
140,550
150,590
EOT
}

done_testing();