FROM perl:5.26

WORKDIR /usr/src/converter-service
COPY cpanfile /usr/src/converter-service/cpanfile

RUN    apt-get update \
    && apt-get -y --no-install-recommends install \
        libpng-dev libtiff-dev libgif-dev libjpeg-dev \
        libgirepository1.0-dev libcairo2-dev libpoppler-glib-dev \
        fonts-crosextra-caladea fonts-crosextra-carlito fonts-croscore \
        unoconv libreoffice-writer libreoffice-calc \
    && apt-get clean \
    && cpanm --installdeps . \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf ~/.cpanm

COPY . /usr/src/converter-service

EXPOSE 5032
CMD [ "bin/run_starman.pl", "--port", "5032", "bin/converter-service.pl" ]
